/*
 Navicat Premium Data Transfer

 Source Server         : Localhost
 Source Server Type    : MySQL
 Source Server Version : 50724
 Source Host           : localhost:3306
 Source Schema         : baseee

 Target Server Type    : MySQL
 Target Server Version : 50724
 File Encoding         : 65001

 Date: 14/06/2021 14:04:30
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for acl_phinxlog
-- ----------------------------
DROP TABLE IF EXISTS `acl_phinxlog`;
CREATE TABLE `acl_phinxlog`  (
  `version` bigint(20) NOT NULL,
  `migration_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `start_time` timestamp(0) NULL DEFAULT NULL,
  `end_time` timestamp(0) NULL DEFAULT NULL,
  `breakpoint` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`version`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of acl_phinxlog
-- ----------------------------
INSERT INTO `acl_phinxlog` VALUES (20141229162641, 'CakePhpDbAcl', '2018-02-01 10:04:00', '2018-02-01 10:04:01', 0);

-- ----------------------------
-- Table structure for acos
-- ----------------------------
DROP TABLE IF EXISTS `acos`;
CREATE TABLE `acos`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NULL DEFAULT NULL,
  `name` varchar(225) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `model` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `foreign_key` int(11) NULL DEFAULT NULL,
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `lft` int(11) NULL DEFAULT NULL,
  `rght` int(11) NULL DEFAULT NULL,
  `status` tinyint(1) NULL DEFAULT 0,
  `sort` int(11) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `lft`(`lft`, `rght`) USING BTREE,
  INDEX `alias`(`alias`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 382 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of acos
-- ----------------------------
INSERT INTO `acos` VALUES (1, NULL, 'controllers', NULL, NULL, 'controllers', 1, 110, 0, 0);
INSERT INTO `acos` VALUES (2, 1, 'Error', NULL, NULL, 'Error', 2, 3, 1, 0);
INSERT INTO `acos` VALUES (3, 1, 'Pages', NULL, NULL, 'Pages', 4, 15, 1, 0);
INSERT INTO `acos` VALUES (19, 1, 'Acl', NULL, NULL, 'Acl', 16, 17, 1, 0);
INSERT INTO `acos` VALUES (20, 1, 'Bake', NULL, NULL, 'Bake', 18, 19, 1, 0);
INSERT INTO `acos` VALUES (21, 1, 'DebugKit', NULL, NULL, 'DebugKit', 20, 55, 1, 0);
INSERT INTO `acos` VALUES (22, 21, 'Composer', NULL, NULL, 'Composer', 21, 24, 1, 0);
INSERT INTO `acos` VALUES (23, 22, 'checkDependencies', NULL, NULL, 'checkDependencies', 22, 23, 1, 0);
INSERT INTO `acos` VALUES (24, 21, 'MailPreview', NULL, NULL, 'MailPreview', 25, 32, 1, 0);
INSERT INTO `acos` VALUES (25, 24, 'index', NULL, NULL, 'index', 26, 27, 1, 0);
INSERT INTO `acos` VALUES (26, 24, 'sent', NULL, NULL, 'sent', 28, 29, 1, 0);
INSERT INTO `acos` VALUES (27, 24, 'email', NULL, NULL, 'email', 30, 31, 1, 0);
INSERT INTO `acos` VALUES (28, 21, 'Panels', NULL, NULL, 'Panels', 33, 38, 1, 0);
INSERT INTO `acos` VALUES (29, 28, 'index', NULL, NULL, 'index', 34, 35, 1, 0);
INSERT INTO `acos` VALUES (30, 28, 'view', NULL, NULL, 'view', 36, 37, 1, 4);
INSERT INTO `acos` VALUES (31, 21, 'Requests', NULL, NULL, 'Requests', 39, 42, 1, 0);
INSERT INTO `acos` VALUES (32, 31, 'view', NULL, NULL, 'view', 40, 41, 1, 4);
INSERT INTO `acos` VALUES (33, 21, 'Toolbar', NULL, NULL, 'Toolbar', 43, 46, 1, 0);
INSERT INTO `acos` VALUES (34, 33, 'clearCache', NULL, NULL, 'clearCache', 44, 45, 1, 0);
INSERT INTO `acos` VALUES (35, 1, 'Migrations', NULL, NULL, 'Migrations', 56, 57, 1, 0);
INSERT INTO `acos` VALUES (56, 1, 'AuditStash', NULL, NULL, 'AuditStash', 58, 59, 1, 0);
INSERT INTO `acos` VALUES (59, 1, 'Josegonzalez\\Upload', NULL, NULL, 'Josegonzalez\\Upload', 60, 61, 1, 0);
INSERT INTO `acos` VALUES (60, 1, 'AppSettings', NULL, NULL, 'AppSettings', 62, 65, 0, 1000);
INSERT INTO `acos` VALUES (61, 60, 'index', NULL, NULL, 'index', 63, 64, 0, 0);
INSERT INTO `acos` VALUES (62, 1, 'Dashboard', NULL, NULL, 'Dashboard', 66, 69, 0, 0);
INSERT INTO `acos` VALUES (63, 62, 'index', NULL, NULL, 'index', 67, 68, 0, 0);
INSERT INTO `acos` VALUES (64, 1, 'Errors', NULL, NULL, 'Errors', 70, 73, 1, 0);
INSERT INTO `acos` VALUES (65, 64, 'unauthorized', NULL, NULL, 'unauthorized', 71, 72, 1, 0);
INSERT INTO `acos` VALUES (73, 3, 'index', NULL, NULL, 'index', 5, 6, 1, 0);
INSERT INTO `acos` VALUES (74, 3, 'logout', NULL, NULL, 'logout', 7, 8, 1, 0);
INSERT INTO `acos` VALUES (75, 3, 'editProfile', NULL, NULL, 'editProfile', 9, 10, 1, 0);
INSERT INTO `acos` VALUES (76, 3, 'activitiesLog', NULL, NULL, 'activitiesLog', 11, 12, 1, 0);
INSERT INTO `acos` VALUES (77, 1, 'Master User', NULL, NULL, 'Users', 74, 85, 0, 101);
INSERT INTO `acos` VALUES (78, 77, 'index', NULL, NULL, 'index', 75, 76, 0, 0);
INSERT INTO `acos` VALUES (79, 77, 'view', NULL, NULL, 'view', 77, 78, 0, 4);
INSERT INTO `acos` VALUES (80, 77, 'add', NULL, NULL, 'add', 79, 80, 0, 1);
INSERT INTO `acos` VALUES (81, 77, 'edit', NULL, NULL, 'edit', 81, 82, 0, 2);
INSERT INTO `acos` VALUES (82, 77, 'delete', NULL, NULL, 'delete', 83, 84, 0, 3);
INSERT INTO `acos` VALUES (89, 3, NULL, NULL, NULL, 'uploadMedia', 13, 14, 1, 0);
INSERT INTO `acos` VALUES (357, 1, 'Master Group', NULL, NULL, 'UserGroups', 92, 105, 0, 102);
INSERT INTO `acos` VALUES (358, 357, 'index', NULL, NULL, 'index', 93, 94, 0, 0);
INSERT INTO `acos` VALUES (359, 357, 'view', NULL, NULL, 'view', 95, 96, 0, 0);
INSERT INTO `acos` VALUES (360, 357, 'add', NULL, NULL, 'add', 97, 98, 0, 0);
INSERT INTO `acos` VALUES (361, 357, 'edit', NULL, NULL, 'edit', 99, 100, 0, 0);
INSERT INTO `acos` VALUES (362, 357, 'delete', NULL, NULL, 'delete', 101, 102, 0, 0);
INSERT INTO `acos` VALUES (363, 357, 'configure', NULL, NULL, 'configure', 103, 104, 0, 0);
INSERT INTO `acos` VALUES (364, 1, NULL, NULL, NULL, 'CakePdf', 106, 107, 1, 0);
INSERT INTO `acos` VALUES (365, 1, NULL, NULL, NULL, 'WyriHaximus\\TwigView', 108, 109, 1, 0);
INSERT INTO `acos` VALUES (378, 21, NULL, NULL, NULL, 'DebugKit', 47, 48, 1, 0);
INSERT INTO `acos` VALUES (379, 21, NULL, NULL, NULL, 'Dashboard', 49, 54, 1, 0);
INSERT INTO `acos` VALUES (380, 379, NULL, NULL, NULL, 'index', 50, 51, 1, 0);
INSERT INTO `acos` VALUES (381, 379, NULL, NULL, NULL, 'reset', 52, 53, 1, 0);

-- ----------------------------
-- Table structure for app_settings
-- ----------------------------
DROP TABLE IF EXISTS `app_settings`;
CREATE TABLE `app_settings`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `keyField` varchar(225) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `valueField` varchar(225) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `type` enum('text','long text','image','select') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of app_settings
-- ----------------------------
INSERT INTO `app_settings` VALUES (1, 'App.Name', 'BASE APP', 'text', 0);
INSERT INTO `app_settings` VALUES (2, 'App.Logo', '/webroot/img/logo.png', 'image', 0);
INSERT INTO `app_settings` VALUES (3, 'App.Logo.Login', '/webroot/img/logo_login.png', 'image', 0);
INSERT INTO `app_settings` VALUES (4, 'App.Logo.Width', '160', 'text', 0);
INSERT INTO `app_settings` VALUES (5, 'App.Logo.Height', '28', 'text', 0);
INSERT INTO `app_settings` VALUES (6, 'App.Logo.Login.Width', '400', 'text', 0);
INSERT INTO `app_settings` VALUES (7, 'App.Logo.Login.Height', '71', 'text', 0);
INSERT INTO `app_settings` VALUES (8, 'App.Login.Cover', '/webroot/assets/img/cover_login.jpg', 'image', 0);
INSERT INTO `app_settings` VALUES (9, 'App.Description', 'BASE APP', 'long text', 0);
INSERT INTO `app_settings` VALUES (10, 'App.Favico', '/webroot/img/favico.png', 'long text', 0);

-- ----------------------------
-- Table structure for aros
-- ----------------------------
DROP TABLE IF EXISTS `aros`;
CREATE TABLE `aros`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NULL DEFAULT NULL,
  `model` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `foreign_key` int(11) NULL DEFAULT NULL,
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `lft` int(11) NULL DEFAULT NULL,
  `rght` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `lft`(`lft`, `rght`) USING BTREE,
  INDEX `alias`(`alias`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of aros
-- ----------------------------
INSERT INTO `aros` VALUES (1, NULL, 'UserGroups', 1, NULL, 1, 4);

-- ----------------------------
-- Table structure for aros_acos
-- ----------------------------
DROP TABLE IF EXISTS `aros_acos`;
CREATE TABLE `aros_acos`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `aro_id` int(11) NOT NULL,
  `aco_id` int(11) NOT NULL,
  `_create` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0',
  `_read` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0',
  `_update` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0',
  `_delete` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `aro_id`(`aro_id`, `aco_id`) USING BTREE,
  INDEX `aco_id`(`aco_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 54 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of aros_acos
-- ----------------------------
INSERT INTO `aros_acos` VALUES (1, 1, 62, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (2, 1, 63, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (4, 1, 67, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (5, 1, 69, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (6, 1, 70, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (7, 1, 71, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (8, 1, 68, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (9, 1, 77, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (10, 1, 78, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (11, 1, 80, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (12, 1, 81, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (13, 1, 82, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (14, 1, 79, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (16, 1, 60, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (17, 1, 61, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (35, 1, 357, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (36, 1, 360, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (37, 1, 359, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (38, 1, 358, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (39, 1, 362, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (40, 1, 361, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (42, 1, 367, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (43, 1, 371, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (44, 1, 370, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (45, 1, 369, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (46, 1, 368, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (47, 1, 363, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (49, 1, 377, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (50, 1, 376, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (51, 1, 375, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (52, 1, 374, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (53, 1, 373, '1', '1', '1', '1');

-- ----------------------------
-- Table structure for audit_logs
-- ----------------------------
DROP TABLE IF EXISTS `audit_logs`;
CREATE TABLE `audit_logs`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timestamp` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  `user_id` int(11) NULL DEFAULT NULL,
  `controller` varchar(225) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `_action` varchar(225) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `type` varchar(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `primary_key` int(11) NULL DEFAULT NULL,
  `source` varchar(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `parent_source` varchar(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `original` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `changed` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `meta` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of audit_logs
-- ----------------------------
INSERT INTO `audit_logs` VALUES (1, '2019-03-18 15:26:49', 1, 'delete', NULL, 'delete', 59964, 'users', NULL, NULL, NULL, '[]');
INSERT INTO `audit_logs` VALUES (2, '2019-03-18 15:26:49', 1, 'delete', NULL, 'delete', 15, 'groups', NULL, NULL, NULL, '[]');
INSERT INTO `audit_logs` VALUES (3, '2019-03-18 15:26:56', 1, 'delete', NULL, 'delete', 59963, 'users', NULL, NULL, NULL, '[]');
INSERT INTO `audit_logs` VALUES (4, '2019-03-18 15:26:56', 1, 'delete', NULL, 'delete', 14, 'groups', NULL, NULL, NULL, '[]');
INSERT INTO `audit_logs` VALUES (5, '2019-03-18 15:27:02', 1, 'delete', NULL, 'delete', 59962, 'users', NULL, NULL, NULL, '[]');
INSERT INTO `audit_logs` VALUES (6, '2019-03-18 15:27:02', 1, 'delete', NULL, 'delete', 11, 'groups', NULL, NULL, NULL, '[]');
INSERT INTO `audit_logs` VALUES (7, '2019-03-18 15:27:10', 1, 'delete', NULL, 'delete', 13, 'groups', NULL, NULL, NULL, '[]');
INSERT INTO `audit_logs` VALUES (8, '2019-03-18 15:27:20', 1, 'delete', NULL, 'delete', 12, 'groups', NULL, NULL, NULL, '[]');
INSERT INTO `audit_logs` VALUES (9, '2019-04-10 21:50:20', 1, 'add', NULL, 'create', 3, 'user_groups', NULL, '{\"id\":3,\"name\":\"Mantap\",\"status\":true,\"created_by\":1}', '{\"id\":3,\"name\":\"Mantap\",\"status\":true,\"created_by\":1}', '[]');
INSERT INTO `audit_logs` VALUES (10, '2019-04-10 21:50:27', 1, 'delete', '3', 'delete', 3, 'user_groups', NULL, NULL, NULL, '[]');
INSERT INTO `audit_logs` VALUES (11, '2019-04-10 22:03:10', 1, 'add', NULL, 'create', 2, 'users', NULL, '{\"id\":2,\"username\":\"farhanriuzaki\",\"name\":\"Farhan Riuzaki\",\"email\":\"riuzakif@gmail.com\",\"user_group_id\":1,\"status\":true,\"created_by\":1}', '{\"id\":2,\"username\":\"farhanriuzaki\",\"name\":\"Farhan Riuzaki\",\"email\":\"riuzakif@gmail.com\",\"user_group_id\":1,\"status\":true,\"created_by\":1}', '[]');
INSERT INTO `audit_logs` VALUES (12, '2019-04-23 10:29:10', 1, 'delete', '2', 'delete', 2, 'users', NULL, NULL, NULL, '[]');

-- ----------------------------
-- Table structure for user_groups
-- ----------------------------
DROP TABLE IF EXISTS `user_groups`;
CREATE TABLE `user_groups`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_by` int(11) NULL DEFAULT NULL,
  `created` datetime(0) NULL DEFAULT NULL,
  `modified_by` int(11) NULL DEFAULT NULL,
  `modified` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of user_groups
-- ----------------------------
INSERT INTO `user_groups` VALUES (1, 'ADMINISTRATOR', 1, 1, '2019-03-26 12:44:05', 1, '2019-03-26 12:44:22');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `password` char(60) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `name` varchar(225) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `email` varchar(225) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `user_group_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_by` int(11) NULL DEFAULT NULL,
  `created` datetime(0) NULL DEFAULT NULL,
  `modified_by` int(11) NULL DEFAULT NULL,
  `modified` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `username`(`username`) USING BTREE,
  INDEX `user_group`(`user_group_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'administrator', '$2y$10$eZFD.eY6CfjVDcPtl6XBy.1Y/5726/ZQz9XKaWP7Jbw/8WXU440be', 'administrator', 'administrator@email.com', 1, 1, 1, '2019-03-26 12:54:54', 1, '2019-03-26 12:54:54');

-- ----------------------------
-- Function structure for SPLIT_STRING
-- ----------------------------
DROP FUNCTION IF EXISTS `SPLIT_STRING`;
delimiter ;;
CREATE FUNCTION `SPLIT_STRING`(`str` VARCHAR(255), `delim` VARCHAR(12), `pos` INT)
 RETURNS varchar(255) CHARSET latin1
RETURN REPLACE(SUBSTRING(SUBSTRING_INDEX(str, delim, pos),
       LENGTH(SUBSTRING_INDEX(str, delim, pos-1)) + 1),
       delim, '');
;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;
